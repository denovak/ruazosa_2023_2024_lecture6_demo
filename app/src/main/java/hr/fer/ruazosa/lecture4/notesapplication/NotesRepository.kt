package hr.fer.ruazosa.lecture4.notesapplication

import android.app.Application
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

class NoteRepository(application: Application) {
    @Database(entities = [Note::class], version = 1)
    @TypeConverters(DateConverter::class)
    abstract class AppDatabase : RoomDatabase() {
        abstract fun noteDao(): NoteDao
    }

    private var database: AppDatabase = Room.databaseBuilder(
        application,
        AppDatabase::class.java, "notes-db"
    ).build()

    fun saveNote(note: Note) {
        database.noteDao().insertAll(note)
    }

    fun updateNote(note: Note) {
        database.noteDao().updateNote(note)
    }

    fun getNoteCount(): Int {
        return database.noteDao().getCount()
    }

    fun getNotes(): List<Note> {
        return database.noteDao().getAll()
    }
    fun deleteNote(note: Note) {
        database.noteDao().deleteNote(note)
    }
}