package hr.fer.ruazosa.lecture4.notesapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import hr.fer.ruazosa.lecture4.notesapplication.databinding.FragmentNoteDetailsBinding
import java.util.Date


class NoteDetailsFragment : Fragment() {
    private var _binding: FragmentNoteDetailsBinding? = null
    private val binding get() = _binding!!
    private val sharedViewModel: NotesViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var noteIndex: Int? = null
        _binding = FragmentNoteDetailsBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.saveNoteButtonId.setOnClickListener {
            noteIndex?.let {
                val note = sharedViewModel.getNote(it)
                note.noteTitle = binding.noteDetailsTitleEditTextId.text.toString()
                note.noteDescription = binding.noteDetailsNoteDescriptionEditTextId.text.toString()
                sharedViewModel.updateNote(note)
            } ?: run {
                val note = Note(noteId = 0, noteDate = Date(), noteTitle = binding.noteDetailsTitleEditTextId.text.toString(),
                    noteDescription = binding.noteDetailsNoteDescriptionEditTextId.text.toString())
                sharedViewModel.saveNote(note)
            }
            val navigationController = findNavController()
            navigationController.popBackStack()
        }

        arguments?.let {
            noteIndex = it.getInt("NOTE_INDEX")
            var note = sharedViewModel.getNote(noteIndex!!)
            binding.noteDetailsTitleEditTextId.setText(note.noteTitle)
            binding.noteDetailsNoteDescriptionEditTextId.setText(note.noteDescription)
        }

        return view
    }
}