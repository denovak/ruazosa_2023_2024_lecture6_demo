package hr.fer.ruazosa.lecture4.notesapplication

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Junction
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Relation
import androidx.room.Transaction
import androidx.room.TypeConverters
import java.util.Date

@Entity(tableName = "notes")
data class Note (
    @PrimaryKey(autoGenerate = true) val noteId: Int,
    @TypeConverters(DateConverter::class)
    @ColumnInfo(name = "note_date") var noteDate: Date,
    @ColumnInfo(name = "note_title") var noteTitle: String,
    @ColumnInfo(name = "note_description") var noteDescription: String)


