package hr.fer.ruazosa.lecture4.notesapplication

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class NotesViewModel(application: Application): AndroidViewModel(application) {
    private lateinit var noteRepository: NoteRepository
    private var repositoryInitialization: Job
    private var listOfNotes = listOf<Note>()

    var repositoryRefreshed = MutableLiveData<Boolean>()

    init {
        repositoryInitialization = viewModelScope.launch(Dispatchers.IO) {
            noteRepository = NoteRepository(application)
        }
    }

    fun refreshRepositoryState() {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryInitialization.join()
            listOfNotes = noteRepository.getNotes()
            repositoryRefreshed.postValue(true)
        }
    }

    fun saveNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepository.saveNote(note)
            refreshRepositoryState()
        }
    }

    fun updateNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            noteRepository.updateNote(note)
            refreshRepositoryState()
        }
    }

    fun getNote(noteAtIndex: Int): Note {
        return listOfNotes[noteAtIndex]
    }

    fun getNoteCount(): Int {
        return listOfNotes.size
    }
    fun deleteNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            repositoryInitialization.join()
            noteRepository.deleteNote(note)
            refreshRepositoryState()
        }
    }

}