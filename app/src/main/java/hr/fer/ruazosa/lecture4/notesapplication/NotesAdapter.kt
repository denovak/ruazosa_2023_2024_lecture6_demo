package hr.fer.ruazosa.lecture4.notesapplication

import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import hr.fer.ruazosa.lecture4.notesapplication.databinding.NoteCellBinding
import java.text.SimpleDateFormat


class NotesAdapter(notesViewModel: NotesViewModel,
                   editItemOnClickListener: OnClickListener,
                   deleteItemOnClickListener: OnClickListener): RecyclerView.Adapter<NotesAdapter.NotesViewHolder>() {
    var notesViewModel = notesViewModel
    var _binding: NoteCellBinding? = null
    private val binding get() = _binding!!
    private val editItemOnClickListener = editItemOnClickListener
    private val deleteItemOnClickListener = deleteItemOnClickListener

    class NotesViewHolder(noteView: View): RecyclerView.ViewHolder(noteView) {
        var noteView = noteView
        var noteDateTextView: TextView = noteView .findViewById(R.id.noteDateTextViewId)
        var noteTitleTextView: TextView = noteView.findViewById(R.id.noteTitleTextViewId)
        var deleteButton: FloatingActionButton = noteView.findViewById(R.id.deleteNoteFloatingActionButtonId)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        _binding = NoteCellBinding.inflate(inflater, parent, false)
        val noteView = binding.root
        noteView.setOnClickListener(editItemOnClickListener)
        val deleteFloatingActionButton = binding.deleteNoteFloatingActionButtonId
        deleteFloatingActionButton.setOnClickListener(deleteItemOnClickListener)
        return NotesViewHolder(noteView)
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        val note = notesViewModel.getNote(position)
        // format date
        val format = SimpleDateFormat("dd.MM.yyyy, HH:mm:ss")
        holder.noteDateTextView.text = format.format(note.noteDate)
        holder.noteTitleTextView.text = note.noteTitle.toString()
        // tag note view to propagate position to listener implementation
        holder.noteView.tag = position
        holder.deleteButton.tag = position
    }

    override fun getItemCount(): Int {
        return notesViewModel.getNoteCount()
    }
}