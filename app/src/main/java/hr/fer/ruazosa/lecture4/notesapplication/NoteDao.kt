package hr.fer.ruazosa.lecture4.notesapplication

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface NoteDao {
    @Query("SELECT * FROM notes")
    fun getAll(): List<Note>

    @Query("SELECT count(*) FROM notes")
    fun getCount(): Int

    @Insert
    fun insertAll(vararg note: Note)

    @Query("SELECT * from notes where noteId = :noteId")
    fun getNoteById(noteId: Int): Note

    @Delete
    fun deleteNote(note: Note)

    @Update
    fun updateNote(note: Note)
}